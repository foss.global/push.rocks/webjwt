/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/webjwt',
  version: '1.0.6',
  description: 'a package to handle jwt in the web'
}
