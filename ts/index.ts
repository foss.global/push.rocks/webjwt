import * as plugins from './webjwt.plugins.js';

export const getDataFromJwtString = <T = any>(jwtString: string): T => {
  const splitted = jwtString.split('.');
  const dataBase64 = splitted[1];
  const plainJsonString = plugins.smartstring.base64.decode(dataBase64);
  return JSON.parse(plainJsonString);
};
