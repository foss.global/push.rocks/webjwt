import { expect, expectAsync, tap } from '@pushrocks/tapbundle';
import * as webjwt from '../ts/index.js';

tap.test('first test', async () => {
  const originalString = JSON.stringify({
    a: {
      complex_2: {
        object: 'yes',
      },
    },
  });
  const stringToDecode = `abc.${btoa(originalString)}.rthgf`;
  const result = webjwt.getDataFromJwtString(stringToDecode);
  expect(result.a['complex_2'].object).toEqual('yes');
  console.log(JSON.stringify(result, null, 2));
});

tap.start();
